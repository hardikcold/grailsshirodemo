package com.example

class Permission {

	String moduleName // Modules name e.g Role, Customer etc.
	String permissionName // Add, Delete etc.
	String permissionString // "role:create,role:dashboard,role:save,role:index" , commad sep string
	//Permission parentModuleName // Role, Customer 
	String description
	Date dateCreated
	Date lastUpdated
	
    static constraints = {
		moduleName nullable : false, blank : false
		permissionName nullable : false, blank : false
		//parentModuleName nullable : true, blank : true
		description nullable : true, blank : true
    }
}
