package com.example

class Role {
    String name
	List previledges
	List permissions
    static hasMany = [ users: User, permissions: String, previledges : Permission]
    static belongsTo = User

    static constraints = {
        name(nullable: false, blank: false, unique: true)
		previledges (nullable: true, blank: true)
    }
}
