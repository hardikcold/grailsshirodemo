package com.example

class RoleController {

    def index() {
		def permissionList = Permission.list()
		permissionList = permissionList.groupBy{it.moduleName}
		
		def roleList = Role.list()
		println "List :" + permissionList
		[permissionList : permissionList, roleList : roleList]	
	}
	
	def save(){
		println "Role Params :" + params
		redirect(action : 'index')
	}
	
	def edit(){
		def roleInstance = Role.read(params?.id)
		def assignedPermissionList = roleInstance?.previledges
		
		def permissionList = Permission.list()
		permissionList = permissionList.groupBy{it.moduleName}
		
		[assignedPermissionList : assignedPermissionList, roleInstance : roleInstance, permissionList : permissionList]
	}
	
	def update(){
		println "Test : " + params?.rolePermission
		def rolePermissionList = params?.rolePermission 
		def roleInstance = Role.read(params?.id)
		
		roleInstance?.previledges.clear()
		roleInstance?.permissions.clear()
		
		rolePermissionList.each{
			def permissionInstance = Permission.read(it as Long)
			println "permissionInstance :" + permissionInstance
			permissionInstance.permissionString.split(",").each{ roleInstance.addToPermissions(it)}
			roleInstance.addToPreviledges(permissionInstance)
		}
		
		roleInstance.save(flush:true)
		
		redirect(action : 'edit', id : params?.id)
	}
}
