import org.apache.shiro.crypto.hash.Sha512Hash

import com.example.Permission
import com.example.Role
import com.example.User

class BootStrap {

    def init = { servletContext ->
		
		def adminRole = new Role(name: "Administrator")
		def permissionInstnace = new Permission(moduleName : "Admin", permissionName : "All Permission", permissionString : "*:*", description : "All Permission").save(flush:true)
		adminRole.addToPermissions("*:*")
		adminRole.addToPreviledges(permissionInstnace)
		adminRole.save(flush:true)
		
		

		
		def userRole = new Role(name:"User")
		def rolePermissionList = generateRolePermissionList()
		rolePermissionList.each{permissionMap->
			permissionInstnace = new Permission(moduleName : permissionMap.moduleName, permissionName : permissionMap.permissionName, permissionString : permissionMap.permissionString, description : permissionMap.description).save(flush:true)
			permissionMap.permissionString.split(",").each{ userRole.addToPermissions(it)}
			userRole.addToPreviledges(permissionInstnace)
		}
		userRole.save(flush:true)

				
		def admin = new User(username: "Admin", passwordHash: new Sha512Hash("password").toHex())
		admin.addToRoles(adminRole)
		admin.save()
		
		def user = new User(username: "User", passwordHash: new Sha512Hash("password").toHex())
		user.addToRoles(userRole)
		user.save()
		
    }
	
	def generateRolePermissionList(){
		def rolePermissionList = []
		rolePermissionList.add([moduleName : 'One', permissionName : 'All', permissionString : "one:*", description : 'One All Role'])
		rolePermissionList.add([moduleName : 'One', permissionName : 'Add', permissionString : "one:create,one:save,one:index", description : 'One Add Role'])
		rolePermissionList.add([moduleName : 'One', permissionName : 'Edit', permissionString : "one:edit,one:update,one:index", description : 'One Edit Role'])
		rolePermissionList.add([moduleName : 'One', permissionName : 'Delete', permissionString : "one:delete,one:index", description : 'One Delete Role'])
		
		rolePermissionList.add([moduleName : 'Two', permissionName : 'All', permissionString : "two:*", description : 'Two All Role'])
		rolePermissionList.add([moduleName : 'Two', permissionName : 'Add', permissionString : "two:create,two:save,two:index", description : 'Two Add Role'])
		rolePermissionList.add([moduleName : 'Two', permissionName : 'Edit', permissionString : "two:edit,two:update,two:index", description : 'Two Edit Role'])
		rolePermissionList.add([moduleName : 'Two', permissionName : 'Delete', permissionString : "two:delete,two:index", description : 'Two Delete Role'])
		
		
		
		return rolePermissionList
	}
	
	def saveDefaultRolePermission(){
		
	}
	
    def destroy = {
    }
}
